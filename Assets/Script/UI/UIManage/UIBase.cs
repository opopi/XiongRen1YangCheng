﻿using UnityEngine;
namespace UiControl
{
    public abstract class UIBase : MonoBehaviour
    {

        /// <summary>
        /// 执行关闭UI的操作
        /// </summary>
        public abstract void DidHideUI();
        /// <summary>
        /// 执行删除UI的操作;
        /// </summary>
        public abstract void DidDestoryUI();
        /// <summary>
        /// 执行显示UI的操作
        /// </summary>
        public abstract void DidShowUI();
        /// <summary>界面状态</summary>
        public ViewState state;

        public struct ViewState
        {
            /// <summary>是否已显示</summary>
            public bool m_SelfShowed;
            /// <summary>层</summary>
            public int Lay;
        }
    }
}
