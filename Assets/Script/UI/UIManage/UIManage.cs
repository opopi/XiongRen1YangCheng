﻿using System.Collections.Generic;
namespace UiControl
{
    public class UIManage : Singleton<UIManage>
    {
     
        /// <summary>键：UI的名字，值：UI的UIBase</summary>
        Dictionary<string, UIBase> uiList = new Dictionary<string, UIBase>();
        /// <summary>
        /// 打开或制造UI界面
        /// </summary>
        /// <param name="UIName">该UI的脚本名</param>
        /// <param name="isUpdate">是否属于需要更新的界面</param>
        public void ShowUI(string UIName, bool isUpdate)
        {
            UIBase uiB = null;
            foreach (string ui in uiList.Keys)//在字典中查找 该界面是否已经初始化
            {
                if (ui == UIName)
                {
                    uiB = uiList[ui];
                    break;
                }
            }
            if (uiB != null)//若已经初始化
            {
                if (uiB.state.m_SelfShowed && !isUpdate)//已经显示 并且不刷新界面
                {
                    
                }
                else
                {
                    uiB.DidShowUI();
                  
                }
                CloseOtherUI( uiB);
            }
            else
            {
                //获取ui 并制造界面
                BuildUI(UIName);
            }
        }
        /// <summary>
        ///从本地路径 加载一个界面到项目中
        /// </summary>
        /// <param name="uiName">界面名称</param>
        public void BuildUI(string uiName)
        {
            //本地固定存储在一个位置 通过界面名称 直接加载进来
        }
        /// <summary>
        /// 关闭界面
        /// </summary>
        /// <param name="closeType">关闭界面的模式</param>
        public void ClosUI(UIBase ui,CloseUIType closeType)
        {
            if(closeType==CloseUIType.Hide)
            {
                HideUIView(ui);
            }
            else if(closeType==CloseUIType.Destroy)
            {
                DestoryUIView(ui);
            }
        }
        /// <summary>
        /// 影藏界面
        /// </summary>
        /// <param name="ui">要影藏的UI</param>
        private void HideUIView(UIBase ui)
        {
            ui.DidHideUI();
        }
        /// <summary>
        /// 销毁界面
        /// </summary>
        /// <param name="ui">要销毁的ui</param>
        private void DestoryUIView(UIBase ui)
        {
            foreach (KeyValuePair<string, UIBase> itemUI in uiList)
            {
                if (itemUI.Value.Equals(ui))
                {
                    uiList.Remove(itemUI.Key);
                }
            }

            ui.DidDestoryUI();
        }
        /// <summary>
        /// 关闭层级大于自己的 已经显示的UI
        /// </summary>
        /// <param name="ui"></param>
        private void CloseOtherUI(UIBase ui)
        {
            foreach (UIBase itemUI in uiList.Values)
            {
                if(itemUI!=ui&&itemUI.state.m_SelfShowed&& itemUI.state.Lay>=ui.state.Lay)//不是自己&&已经显示的&&层级大于等于自己的
                {
                    HideUIView(itemUI);
                }
            }
        }
        private void CloseAllUI()
        {
            foreach (UIBase itemUI in uiList.Values)
            {
                itemUI.DidHideUI();
            }
        }
    }
  
    /// <summary>
    /// 关闭界面的模式 （影藏，删除）
    /// </summary>
    public enum CloseUIType
    {
        Hide,
        Destroy
    }
}
